# FMS checker

## Словарь

- *Паспорт* - запись в формате  code, number (серия и номер паспорта).

- *FMS Server* - База ФМС недействительных паспортов.

- *Database* - база данных для хранения данных о недействительных паспортах.

- *Importer* - сервис для импорта данных о недействительных паспортах в Database.
  
- *Downloader* - сервис для скачивания данных из third-party сервиса ФМС.

- *Checker* - сервис для проверки данных конкретного паспорта.

- *Scheduler* - сторонний сервис-планировщик (возможно использовать просто cron-job, в зависимости от требований связанных с актуальностью данных)

- *Internal Services* - любые сервисы компании.

## Схемы работы сервиса

```puml
component [Downloader]
component [Scheduler]
component [Importer]
component [Checker]
component [Internal Service]
cloud {
  [FMS Server]
}


database "Memcached" {
  folder Database{
    [code+number]
  }
}

[FMS Server] ..>[Downloader]
[Downloader] --> [S3 Storage]
[S3 Storage] --> [Importer]
[Downloader] --> [Scheduler]
[Importer] --> [code+number]
[code+number] -> [Checker]
[Checker] ..> HTTP
HTTP ..> [Internal Service]
```

![Component diagram](http://www.plantuml.com/plantuml/png/RL2zJiGm3Dxp55PNr2xiNQWG39MGqgrYI1D34McCSkdTSEgx4rU8f56NoD_FvzZMjFoGGnTWxEZcB2c3_brajDGpcjKMx6kiV2Z8fndrTsbqqUMK1Od--gKnYfPM0tS1C3xtSkSvimaXZ0fgKXU4giT5A_wvsfqVP0rwwFvqfc9Say47jouJvm4sxf7tXAPf3slb0EgwZTizWWparMSSB0UifYrFa-akoSTICTS1GPJJcVuh7XHUXk5DyCFex72_)

### Check passport from internal service

```puml
"Internal Services" -> "Checker" : Запрос на проверку данных\n@ описать запрос
"Checker" -> "database"
"database" -> "Checker"
"Checker" -> "Internal Services" : Результат поиска\n @ описать ответ
```
![Checker](http://www.plantuml.com/plantuml/png/PSyn2i9040NGFgSOjlS25Y9OMTkcMUE0ecoH16kpmShqjXvX4KCaaiqL_jn84ukWgNO7Xl_VgAL9EJRwG2kEZxk84qMJ6Qd5bgCznugcX2iSMZd1IqPeuEWxuOxoyzQI4nvmQD38HSwXcHCyMZmbWnCh1Q4Q4e8XkklPw5IlTS8g6Bu_WB_zCRSJtb2YaXmlASHsfOGMlXVKSA6XCScBxG-mmHi0)
### Load data from FMS

```puml
"Downloader" -> "FMS Server"
"Downloader" <- "FMS Server"
"Downloader" -> "Scheduler" : Задача в планировщик загрузки @todo\n etag and expires from response
```
![Downloader](http://www.plantuml.com/plantuml/png/ur9ooI_FoybFJ4ajKbBGjLDGSlCDLWXEBIe3yb4bRNHH935r1YTdfAQKve2uLWeNfb_OS67BXGqNsoziKBYmIU72_WkxWI9xB-ous71XtuLD5pilxBYmI-72Tg3WveiD5vk1h5qNTYWubEIdvCVaAQIM9AOh9EQbAAHM56GMfHOhf1Nbvoe06GNvUSMf0000)